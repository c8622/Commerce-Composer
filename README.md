# Commerce code snippets template

This repository is used by [Commerce-Accelerator](https://github.com/Ekatereana/commerce-accellerator) as code snippets source\

Each branch represents example of template for commerce apps as landing pages, function apps, etc.

Language of the templates: 

- Typescript
- Javascript
- HTML + SCSS

### Available templates

- azure/http-trigger -- basic implementation of typescript azure trigger
- any/landing-page/knight -- landing page html design snippet
- any/landing-page/solid -- landing page html design snippet




